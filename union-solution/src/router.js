import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/consultas",
      name: "consultas",
      component: () => import("./components/ConsultasList")
    },
    {
      path: "/veiculoconsulta",
      name: "veiculo-consulta",
      component: () => import("./components/VeiculoConsulta")
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./components/Login")
    },
    {
      path: "/logoff",
      name: "logoff",
      redirect: { name: 'login' }
    }
  ]
});
