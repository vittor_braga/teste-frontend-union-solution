import axios from "axios";

export default axios.create({
  baseURL: "https://api.treinamento.carzen.com.br",
  headers: {
    "Content-type": "application/json",
    "Authorization": window.localStorage.getItem('api_token')
  }
});
