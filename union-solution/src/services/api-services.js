import http from "../http-common";

class ApiService {
  login(data) {
    return http.post('/user/login', data)
  }

  listConsult() {
    return http.get('/vehicle/listConsult');
  }

  vehiclePlate(placa) {
    return http.get('/vehicle/plate/'+placa);
  }
}

export default new ApiService();