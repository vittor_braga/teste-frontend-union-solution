# union-solution

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Observacoes
```
Diversas coisas deveriam ser feitas nesse projeto, vou listar algumas coisas:
- Layout, não foi feito nada relacionado a layout, a não ser usar o bootstrap
- Por ter uma resposta de erro padrão poderia ter uma função de tratamento de erros ao invés de tratar em cada chamada
- Validações locais, como o tamanho e formato da string da placa, para evitar idas desnecessárias ao servidor
- Uso de constantes como a chave do armazenamento do token no local storage.
Como fiz durante o meu almoço não consegui concluir todos esses requisitos que seria importantes em um projeto real
```